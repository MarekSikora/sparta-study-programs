<?php
/* GET request */
header("Access-Control-Allow-Origin: *");

$output = new stdClass();
$output->error = false;
$output->message = "ok";


// ------ dočasný disabled
	// $output->error = false;
	// $output->message = "Function disabled";
	// $response = json_encode($output);
	// echo $response;
	// exit();
// -------------

if (isset($_GET["p"])){

	$inputObj = json_decode(base64_decode($_GET["p"]));
	
	if (property_exists($inputObj, "name") && property_exists($inputObj, "action")) {
		if (strlen($inputObj->name) < 1) {
			$output->error = true;
			$output->message = "Can not read the data!";	
		} 
	} else {
		$output->error = true;
		$output->message = "Can not read the data!";	
	}
	
	if (!$output->error){	
		
		if ($inputObj->action == "request") {
			$fileName = './universities/add-requests/'.$inputObj->name.'.txt';
		} else {
			$fileName = './universities/'.$inputObj->name.'.txt';
		}
			
		//smazani univerzity
		if (!$output->error && file_exists($fileName)){
			if (unlink($fileName)) {
				$output->message = "Successed!";
			} else {
				$output->error = true;
				$output->message = "Can not unlink the data!";
			}
		} else {
			$output->error = true;
			$output->message = "Error: Can not find the university in the database!";
		}
		
	}
	
	$response = json_encode($output);
	echo $response;

} else {
	http_response_code(403);
}

?>

