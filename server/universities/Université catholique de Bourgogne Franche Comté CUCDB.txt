{
    "name": "Universit\u00e9 catholique de Bourgogne Franche Comt\u00e9 CUCDB",
    "country": "France",
    "cor": {
        "lat": 47.334912,
        "lng": 5.053975
    },
    "world_university_rankings": "",
    "programs": [
        {
            "study_program": "Expert en syst\u00e8me informatique",
            "department": "Diiage",
            "degree": "Master",
            "degree_title": "Expert en syst\u00e8me informatique",
            "language": [
                "French",
                "English"
            ],
            "duration": "2 years",
            "cost": "10100",
            "link": "http:\/\/diiage.cucdb.fr",
            "practical_lectures": 25,
            "percentage_of_subjects_on": {
                "computer_science": 40,
                "cryptography": 20,
                "humanistic": 10,
                "mathematics": 0,
                "privacy": 10,
                "security": 20
            },
            "note": "Data collected in academic year 2019-2020"
        }
    ],
    "created": {
        "username": "admin",
        "usermail": "",
        "userphone": "",
        "timestamp": "2020-11-01 00:00:00"
    }
}