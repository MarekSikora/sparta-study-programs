{
    "name": "University of Bonn",
    "country": "Germany",
    "cor": {
        "lat": 50.727057,
        "lng": 7.083336
    },
    "world_university_rankings": "110th",
    "programs": [
        {
            "study_program": "Bachelor Cyber Security",
            "department": "Computer Science",
            "degree": "Bachelor",
            "degree_title": "Bachelor of Science",
            "language": [
                "German",
                "English"
            ],
            "duration": "3 years",
            "cost": "1680",
            "link": "https:\/\/www.informatik.uni-bonn.de\/de\/fuer-studierende\/bachelorstudiengang-cyber-security",
            "practical_lectures": 0,
            "percentage_of_subjects_on": {
                "computer_science": 48.53,
                "cryptography": 1.47,
                "humanistic": 13.24,
                "mathematics": 20.59,
                "privacy": 7.35,
                "security": 8.82
            },
            "note": "Data collected in academic year 2019-2020"
        }
    ],
    "created": {
        "username": "admin",
        "usermail": "",
        "userphone": "",
        "timestamp": "2020-11-01 00:00:00"
    }
}