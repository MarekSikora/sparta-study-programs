{
    "name": "Korea University",
    "country": "South Korea",
    "cor": {
        "lat": 37.591045,
        "lng": 127.027247
    },
    "world_university_rankings": "198",
    "programs": [
        {
            "study_program": "Information Security Convergence",
            "department": "Graduate School of Information Security",
            "degree": "Master",
            "degree_title": "",
            "language": [
                "English"
            ],
            "duration": "2 years",
            "cost": "",
            "link": "https:\/\/infosecurity.korea.ac.kr\/infosecurity\/major\/Major.do",
            "practical_lectures": 25,
            "percentage_of_subjects_on": {
                "computer_science": 58.33,
                "cryptography": 16.67,
                "humanistic": 0,
                "mathematics": 0,
                "privacy": 0,
                "security": 25
            },
            "note": "Data collected in academic year 2019-2020"
        }
    ],
    "created": {
        "username": "admin",
        "usermail": "",
        "userphone": "",
        "timestamp": "2020-11-01 00:00:00"
    }
}