{
    "name": "Grenoble Alpes University",
    "country": "France",
    "cor": {
        "lat": 45.191329,
        "lng": 5.76738
    },
    "world_university_rankings": "301-350",
    "programs": [
        {
            "study_program": "Information Technology & Security (IT&S) specialization",
            "department": "Grenoble INP Institute of Engineering and Management - Esisar",
            "degree": "Master",
            "degree_title": "Engineer \u201cInformation Technology & Security\u201d",
            "language": [
                "French"
            ],
            "duration": "3 years",
            "cost": "601",
            "link": "https:\/\/esisar.grenoble-inp.fr\/en\/academics\/engineering-degree-grenoble-inp-esisar-information-technology-it#page-presentation",
            "practical_lectures": 100,
            "percentage_of_subjects_on": {
                "computer_science": 33,
                "cryptography": 8,
                "humanistic": 14,
                "mathematics": 8,
                "privacy": 3,
                "security": 34
            },
            "note": "Data collected in academic year 2019-2020"
        }
    ],
    "created": {
        "username": "admin",
        "usermail": "",
        "userphone": "",
        "timestamp": "2020-11-01 00:00:00"
    }
}