{
    "name": "Aalto University",
    "country": "Finland",
    "cor": {
        "lat": 60.184839,
        "lng": 24.82248
    },
    "programs": [
        {
            "study_program": "Master's Programme in Security and Cloud Computing",
            "department": "Department of Computer Science",
            "degree": "Master",
            "degree_title": "Master of Science",
            "language": [
                "English"
            ],
            "duration": "2 years",
            "cost": "9000",
            "link": "https:\/\/secclo.eu\/programme\/programme\/",
            "practical_lectures": 50,
            "percentage_of_subjects_on": {
                "computer_science": 68,
                "cryptography": 4,
                "humanistic": 14,
                "mathematics": 0,
                "privacy": 4,
                "security": 10
            },
            "note": "Data collected in academic year 2019-2020"
        }
    ],
    "world_university_rankings": "184",
    "created": {
        "username": "admin",
        "usermail": "",
        "userphone": "",
        "timestamp": "2020-11-01 00:00:00"
    }
}