{
    "name": "T\u00e9l\u00e9com SudParis (Institut Mines T\u00e9l\u00e9com)",
    "country": "France",
    "cor": {
        "lat": 48.62493,
        "lng": 2.443382
    },
    "world_university_rankings": "",
    "programs": [
        {
            "study_program": "Systems and Network Security",
            "department": "Network and Security",
            "degree": "Master",
            "degree_title": "T\u00e9l\u00e9com SudParis Engineer \u2013 specialty in Systems and Network Security",
            "language": [
                "French"
            ],
            "duration": "3 years",
            "cost": "8000",
            "link": "https:\/\/ssr.telecom-sudparis.eu\/",
            "practical_lectures": 60,
            "percentage_of_subjects_on": {
                "computer_science": 17,
                "cryptography": 9,
                "humanistic": 7,
                "mathematics": 0,
                "privacy": 2,
                "security": 65
            },
            "note": "Data collected in academic year 2019-2020"
        }
    ],
    "created": {
        "username": "admin",
        "usermail": "",
        "userphone": "",
        "timestamp": "2020-11-01 00:00:00"
    }
}