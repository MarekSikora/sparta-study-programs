{
    "name": "Efrei Paris",
    "country": "France",
    "cor": {
        "lat": 48.78884,
        "lng": 2.363765
    },
    "world_university_rankings": "",
    "programs": [
        {
            "study_program": "Majeure Cybers\u00e9curit\u00e9",
            "department": "Cycle M",
            "degree": "Master",
            "degree_title": "Dipl\u00f4me d\u2019ing\u00e9nieur de l\u2019EFREI",
            "language": [
                "French"
            ],
            "duration": "2 years",
            "cost": "9320",
            "link": "https:\/\/www.efrei.fr\/programme-grande-ecole\/le-cycle-ingenieur\/m1-m2-ingenieur-securite-si\/",
            "practical_lectures": 75,
            "percentage_of_subjects_on": {
                "computer_science": 20,
                "cryptography": 10,
                "humanistic": 0,
                "mathematics": 0,
                "privacy": 25,
                "security": 45
            },
            "note": "Data collected in academic year 2019-2020"
        }
    ],
    "created": {
        "username": "admin",
        "usermail": "",
        "userphone": "",
        "timestamp": "2020-11-01 00:00:00"
    }
}