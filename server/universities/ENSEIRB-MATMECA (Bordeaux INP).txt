{
    "name": "ENSEIRB-MATMECA (Bordeaux INP)",
    "country": "France",
    "cor": {
        "lat": 44.806693,
        "lng": -0.60516
    },
    "world_university_rankings": "",
    "programs": [
        {
            "study_program": "Cyber security, systems and networking",
            "department": "Computer Science",
            "degree": "Master",
            "degree_title": "Ing\u00e9nieur en Informatique option cybeR-s\u00e9curit\u00e9, Syst\u00e8mes et R\u00e9seaux (RSR)",
            "language": [
                "French"
            ],
            "duration": "3 years",
            "cost": "600",
            "link": "http:\/\/rsr-enseirb-matmeca.fr\/",
            "practical_lectures": 50,
            "percentage_of_subjects_on": {
                "computer_science": 20,
                "cryptography": 4,
                "humanistic": 0,
                "mathematics": 4,
                "privacy": 0,
                "security": 72
            },
            "note": "Data collected in academic year 2019-2020"
        }
    ],
    "created": {
        "username": "admin",
        "usermail": "",
        "userphone": "",
        "timestamp": "2020-11-01 00:00:00"
    }
}