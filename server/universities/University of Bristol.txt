{
    "name": "University of Bristol",
    "country": "United Kingdom",
    "cor": {
        "lat": 51.45841,
        "lng": -2.602883
    },
    "world_university_rankings": "78th",
    "programs": [
        {
            "department": "School of Mathematics",
            "degree": "Master",
            "degree_title": "",
            "language": [
                "English"
            ],
            "duration": "1 year",
            "cost": "16400",
            "link": "http:\/\/www.bris.ac.uk\/study\/postgraduate\/2019\/sci\/msc-mathematics-of-cybersecurity\/",
            "practical_lectures": 25,
            "percentage_of_subjects_on": {
                "computer_science": 6.25,
                "cryptography": 0,
                "humanistic": 0,
                "mathematics": 31.25,
                "privacy": 43.75,
                "security": 18.75
            },
            "study_program": "Master in Mathematics of Cybersecurity",
            "note": "Data collected in academic year 2019-2020"
        }
    ],
    "created": {
        "username": "admin",
        "usermail": "",
        "userphone": "",
        "timestamp": "2020-11-01 00:00:00"
    }
}