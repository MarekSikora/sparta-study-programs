{
    "name": "IMT Atlantique",
    "country": "France",
    "cor": {
        "lat": 48.120399,
        "lng": -1.628616
    },
    "world_university_rankings": "351 - 400th",
    "programs": [
        {
            "study_program": "Mast\u00e8re Sp\u00e9cialis\u00e9 CyberS\u00e9curit\u00e9",
            "department": "SRCD (for IMT Atlantique)",
            "degree": "Master",
            "degree_title": "Mast\u00e8re Sp\u00e9cialis\u00e9 CyberS\u00e9curit\u00e9",
            "language": [
                "French"
            ],
            "duration": "other",
            "cost": "17 500 ",
            "link": "https:\/\/www.imt-atlantique.fr\/fr\/formation\/masteres-specialises\/mastere-specialise-en-cybersecurite",
            "practical_lectures": 75,
            "percentage_of_subjects_on": {
                "computer_science": 42.8,
                "cryptography": 9.13,
                "humanistic": 14.9,
                "mathematics": 4.33,
                "privacy": 0.96,
                "security": 27.88
            },
            "note": "Data collected in academic year 2019-2020"
        }
    ],
    "created": {
        "username": "admin",
        "usermail": "",
        "userphone": "",
        "timestamp": "2020-11-01 00:00:00"
    }
}