{
    "name": "Technical University of Darmstadt",
    "country": "Germany",
    "cor": {
        "lat": 49.874938,
        "lng": 8.656071
    },
    "world_university_rankings": "251 - 300th",
    "programs": [
        {
            "study_program": "IT security",
            "department": "Computer Science",
            "degree": "Master",
            "degree_title": "Master of Science",
            "language": [
                "German",
                "English"
            ],
            "duration": "2 years",
            "cost": "1080",
            "link": "https:\/\/www.tu-darmstadt.de\/studieren\/studieninteressierte\/studienangebot_studiengaenge\/studiengang_183744.de.jsp",
            "practical_lectures": "",
            "percentage_of_subjects_on": {
                "computer_science": 16.7,
                "cryptography": 41.7,
                "humanistic": 0,
                "mathematics": 8.3,
                "privacy": 8.3,
                "security": 25
            },
            "note": "Data collected in academic year 2019-2020"
        }
    ],
    "created": {
        "username": "admin",
        "usermail": "",
        "userphone": "",
        "timestamp": "2020-11-01 00:00:00"
    }
}