{
    "name": "The University of Edinburgh",
    "country": "United Kingdom",
    "cor": {
        "lat": 55.944552,
        "lng": -3.189338
    },
    "world_university_rankings": "29th",
    "programs": [
        {
            "study_program": "Cyber Security, Privacy and Trust",
            "department": "Informatics",
            "degree": "Master",
            "degree_title": "MSc",
            "language": [
                "English"
            ],
            "duration": "1 year",
            "cost": "15500",
            "link": "https:\/\/www.ed.ac.uk\/studying\/postgraduate\/degrees\/index.php?r=site\/view&edition=2019&id=971",
            "practical_lectures": 50,
            "percentage_of_subjects_on": {
                "computer_science": 0,
                "cryptography": 33.3,
                "humanistic": 0,
                "mathematics": 16.7,
                "privacy": 8.3,
                "security": 41.7
            },
            "note": "Data collected in academic year 2019-2020"
        }
    ],
    "created": {
        "username": "admin",
        "usermail": "",
        "userphone": "",
        "timestamp": "2020-11-01 00:00:00"
    }
}