{
    "name": "ISEP",
    "country": "France",
    "cor": {
        "lat": 48.824615,
        "lng": 2.279922
    },
    "world_university_rankings": "",
    "programs": [
        {
            "study_program": "Architecture Cybers\u00e9curit\u00e9 et Int\u00e9gration",
            "department": "Formation continue",
            "degree": "Master",
            "degree_title": "Mast\u00e8re Sp\u00e9cialis\u00e9",
            "language": [
                "French"
            ],
            "duration": "1 year",
            "cost": "12000",
            "link": "https:\/\/formation-continue.isep.fr\/architecture-cyber-securite-et-integration\/",
            "practical_lectures": 25,
            "percentage_of_subjects_on": {
                "computer_science": 20.8,
                "cryptography": 2.8,
                "humanistic": 38.9,
                "mathematics": 0,
                "privacy": 5.6,
                "security": 31.9
            },
            "note": "Data collected in academic year 2019-2020"
        }
    ],
    "created": {
        "username": "admin",
        "usermail": "",
        "userphone": "",
        "timestamp": "2020-11-01 00:00:00"
    }
}