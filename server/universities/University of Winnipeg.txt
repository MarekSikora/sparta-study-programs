{
    "name": "University of Winnipeg",
    "country": "Canada",
    "cor": {
        "lat": 49.891261,
        "lng": -97.153326
    },
    "world_university_rankings": "",
    "programs": [
        {
            "study_program": "Network Security Diploma",
            "department": "Professional, Applied & Continuing Education (PACE)",
            "degree": "Master",
            "degree_title": "Master of Network Security (Network Security Diploma)",
            "language": [
                "English"
            ],
            "duration": "2 years",
            "cost": "14333",
            "link": "http:\/\/pace.uwinnipegcourses.ca\/full-time-programs\/network-security-diploma",
            "practical_lectures": "",
            "percentage_of_subjects_on": {
                "computer_science": 13.7,
                "cryptography": 0,
                "humanistic": 46,
                "mathematics": 0,
                "privacy": 0,
                "security": 40.3
            },
            "note": "Data collected in academic year 2019-2020"
        }
    ],
    "created": {
        "username": "admin",
        "usermail": "",
        "userphone": "",
        "timestamp": "2020-11-01 00:00:00"
    }
}