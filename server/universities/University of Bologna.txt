{
    "name": "University of Bologna",
    "country": "Italy",
    "cor": {
        "lat": 44.489664,
        "lng": 11.338998
    },
    "world_university_rankings": "180th",
    "programs": [
        {
            "study_program": "Master in digital technology management\/cyber security",
            "department": "Bologna Business School",
            "degree": "Master",
            "degree_title": "",
            "language": [
                "English"
            ],
            "duration": "1 year",
            "cost": "14800",
            "link": "https:\/\/www.bbs.unibo.eu\/hp\/master-fulltime\/digital-technology-management-cyber-security-2\/#presentazione",
            "practical_lectures": "",
            "percentage_of_subjects_on": {
                "computer_science": 6.3,
                "cryptography": 3.1,
                "humanistic": 57.8,
                "mathematics": 0,
                "privacy": 4.7,
                "security": 28.1
            },
            "note": "Data collected in academic year 2019-2020"
        }
    ],
    "created": {
        "username": "admin",
        "usermail": "",
        "userphone": "",
        "timestamp": "2020-11-01 00:00:00"
    }
}