{
    "name": "EURECOM",
    "country": "France",
    "cor": {
        "lat": 43.614449,
        "lng": 7.071347
    },
    "world_university_rankings": "551\/600 in Computer Science & Information Systems.",
    "programs": [
        {
            "study_program": "Digital Security",
            "department": "Digital Security",
            "degree": "Master",
            "degree_title": "Master of Science in Digital Security",
            "language": [
                "English"
            ],
            "duration": "2 years",
            "cost": "6000",
            "link": "https:\/\/www.eurecom.fr\/en\/teaching\/master-science\/master-degree-digital-security",
            "practical_lectures": 50,
            "percentage_of_subjects_on": {
                "computer_science": 20,
                "cryptography": 10,
                "humanistic": 10,
                "mathematics": 10,
                "privacy": 10,
                "security": 40
            },
            "note": "Data collected in academic year 2019-2020"
        },
        {
            "study_program": "Security in Computer Systems and Communications",
            "department": "Digital Security",
            "degree": "Master",
            "degree_title": "Post Master Degree in Security in Computer Systems and Communications",
            "language": [
                "English"
            ],
            "duration": "other",
            "cost": "6000",
            "link": "https:\/\/www.eurecom.fr\/en\/teaching\/post-master-degree\/security-computer-systems-and-communications ",
            "practical_lectures": 50,
            "percentage_of_subjects_on": {
                "computer_science": 20,
                "cryptography": 10,
                "humanistic": 10,
                "mathematics": 10,
                "privacy": 10,
                "security": 40
            },
            "note": "Data collected in academic year 2019-2020"
        }
    ],
    "created": {
        "username": "admin",
        "usermail": "",
        "userphone": "",
        "timestamp": "2020-11-01 00:00:00"
    }
}