{
    "name": "FH JOANNEUM GmbH",
    "country": "Austria",
    "cor": {
        "lat": 47.069351,
        "lng": 15.409746
    },
    "world_university_rankings": "NA",
    "programs": [
        {
            "study_program": "IT & Mobile Security",
            "department": "Internet Technologies & Applications",
            "degree": "Master",
            "degree_title": "MSc",
            "language": [
                "German",
                "English"
            ],
            "duration": "2 years",
            "cost": "0",
            "link": "https:\/\/www.fh-joanneum.at\/it-und-mobile-security\/master\/en\/",
            "practical_lectures": 75,
            "percentage_of_subjects_on": {
                "computer_science": 25.5,
                "cryptography": 10.2,
                "humanistic": 15.9,
                "mathematics": 3.8,
                "privacy": 5.4,
                "security": 39.2
            },
            "note": "Data collected in academic year 2019-2020"
        }
    ],
    "created": {
        "username": "admin",
        "usermail": "",
        "userphone": "",
        "timestamp": "2020-11-01 00:00:00"
    }
}