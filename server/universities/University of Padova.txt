{
    "created": {
        "username": "Mauro Conti",
        "usermail": "cybersecurity@math.unipd.it",
        "userphone": "",
        "timestamp": "2021-9-17 13:31:24"
    },
    "name": "University of Padova",
    "country": "Italy",
    "cor": {
        "lat": 45.4126130741686,
        "lng": 11.887354072671585
    },
    "world_university_rankings": "165 Center for World University Rankings \u2013 CWUR",
    "programs": [
        {
            "study_program": "Cybersecurity",
            "department": "Department of Mathematics",
            "degree": "Master",
            "degree_title": "Cybersecurity",
            "language": [
                "English"
            ],
            "duration": "2 years",
            "cost": "2.622",
            "link": "https:\/\/cybersecurity.math.unipd.it\/",
            "note": "Data collected in academic year 2020-2021",
            "practical_lectures": 30,
            "percentage_of_subjects_on": {
                "computer_science": 25,
                "cryptography": 5,
                "humanistic": 10,
                "mathematics": 20,
                "privacy": 10,
                "security": 30
            }
        }
    ]
}