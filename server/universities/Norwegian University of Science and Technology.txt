{
    "name": "Norwegian University of Science and Technology",
    "country": "Norway",
    "cor": {
        "lat": 63.419336,
        "lng": 10.402351
    },
    "world_university_rankings": "351 \u2013 400",
    "programs": [
        {
            "study_program": "Information Security",
            "department": "Faculty of Information Technology and Electrical Engineering.",
            "degree": "Master",
            "degree_title": "Master in Information Security",
            "language": [
                "English"
            ],
            "duration": "2 years",
            "cost": "0",
            "link": "https:\/\/www.ntnu.edu\/studies\/mis",
            "practical_lectures": 20,
            "percentage_of_subjects_on": {
                "computer_science": 22,
                "cryptography": 10,
                "humanistic": 28,
                "mathematics": 0,
                "privacy": 5,
                "security": 35
            },
            "note": "Data collected in academic year 2019-2020"
        }
    ],
    "created": {
        "username": "admin",
        "usermail": "",
        "userphone": "",
        "timestamp": "2020-11-01 00:00:00"
    }
}