{
    "name": "Warsaw University of Technology",
    "country": "Poland",
    "cor": {
        "lat": 52.218832458,
        "lng": 21.006333308
    },
    "world_university_rankings": "1001+",
    "programs": [
        {
            "study_program": "Cybersecurity",
            "department": "The Faculty of Electronics and Information Technology, The Institute of Telecommunications, Division of Cybersecurity",
            "degree": "Bachelor",
            "degree_title": "Bachelor of Science in Engineering in the field of Cybersecurity",
            "language": [
                "English",
                "Polish"
            ],
            "duration": "other",
            "cost": "",
            "link": "https:\/\/usosweb.usos.pw.edu.pl\/kontroler.php?_action=katalog2%2Fprogramy%2FpokazProgram&prg_kod=103B-ISP-CB&lang=en",
            "practical_lectures": 100,
            "percentage_of_subjects_on": {
                "humanistic": 0,
                "mathematics": 6.2,
                "privacy": 0,
                "security": 51.3,
                "computer_science": 15,
                "cryptography": 27.5
            },
            "note": "Data collected in academic year 2019-2020"
        }
    ],
    "created": {
        "username": "admin",
        "usermail": "",
        "userphone": "",
        "timestamp": "2020-11-01 00:00:00"
    }
}