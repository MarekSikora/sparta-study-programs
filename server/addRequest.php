<?php
/* POST request */
/* -------- HTTP HEADER -------- */
header("Access-Control-Allow-Origin: *");
/* -------------------------------- */



/* -------- ERROR LOGGING -------- */
$logfilename = "addrequest-".date("Ymd_His");
if (file_exists("log/".$logfilename.".txt")) {
	$append = 1;
	while (file_exists("log/".$logfilename."_".$append.".txt")) {
		$append++;
	}
	$logfilename .= "_".$append;
}

$logfile = fopen("log/".$logfilename.".txt", "a");




$output = new stdClass();
$output->error = false;
$output->message = "ok";

$json = file_get_contents('php://input');
fwrite($logfile, $json);
fclose($logfile);

$files = glob('./universities/*.txt', GLOB_BRACE);
$requests = glob('./universities/add-requests/*.txt', GLOB_BRACE);

if (!$json){
	$output->error = true;
	$output->message = "Can not read the data!";
}
/* -------------------------------- */


// ------ dočasný disabled
	// $output->error = false;
	// $output->message = "Function disabled";
	// $response = json_encode($output);
	// echo $response;
	// exit();
// -------------

/* -------- INPUT DATA READ -------- */
if (!$output->error){
	$obj = json_decode($json);
	if (property_exists($obj, "universities")) {
		$newData = json_decode($obj->{"universities"});
		if (property_exists($newData, "id")) unset($newData->id);
		if (!property_exists($newData, "created") || !property_exists($newData, "name") || !property_exists($newData, "programs") || !property_exists($newData, "cor") || !property_exists($newData, "country")) {
			$output->error = true;
			$output->message = "Can not read the data!";
		}
	} else {
		$output->error = true;
		$output->message = "Can not read the data!";
	}
}
/* -------------------------------- */


/* -------- DATA IMPORT ---------- */
if (!$output->error){
	$newName = $newData->name;
	
	// oveření, že soubor už neexistuje
	$exists = false;
	foreach($files as $file){
		if (strpos($file, $newName) !== false) {
			$exists = true;
			break;
		}
	}	
	
	foreach($requests as $file){
		if (strpos($file, $newName) !== false) {
			$exists = true;
			break;
		}
	}	
	
	if ($exists) {
		$output->error = true;
		$output->message = "Can not save new university! A university with the same name already exists.";
	} else {
		
		// vytvoření nového txt
		$newFile = fopen("universities/add-requests/".$newName.".txt", "w");
		if (!$newFile){
			$output->error = true;
			$output->message = "Can not create new txt files!";
		} else {
		
			// vložení json dat
			fwrite($newFile, json_encode($newData, JSON_PRETTY_PRINT));
			fclose($newFile);
			
			// mailové upozornění
			
			/*
			// method 1 - PHPMailer
			
			use PHPMailer\PHPMailer\PHPMailer;
			use PHPMailer\PHPMailer\Exception;
			require 'phpmailer/Exception.php';
			require 'phpmailer/PHPMailer.php';
			require 'phpmailer/SMTP.php';

			$mail = new PHPMailer(true);

			$mail->From = 'webmaster@informacni-bezpecnost.cz';
			$mail->FromName = 'Cybersecurity Study Programs App';
			$mail->addAddress("mara000@seznam.cz");
			$mail->addReplyTo('marek.sikora@vutbr.cz', 'Marek');

			$mail->Subject = "New university request";
			$mail->Body    = "New university requests to add to the Study Programs application.\nConfirm the request here: https://informacni-bezpecnost.cz/study-programs/admin \n\n";
			$mail->Body   .= "University: ".$newName."\n";
			$mail->Body   .= "Requested by: ".$newData->created->username."\n";
			$mail->Body   .= "E-mail: ".$newData->created->username."\n";
			$mail->Body   .= "Phone: ".$newData->created->username."\n";

			if(!$mail->send()) {
				$output->email = 'Mailer Error: ' . $mail->ErrorInfo;
			} else {
				$output->email = 'Mail has been sent';
			}
			*/
			
			
			// method 2 - PHP mail()
			// $to      = 	"mara000@seznam.cz";
			$to      = 	"crypto@feec.vutbr.cz";			
			$subject = 	"New university request";
			$message =	"New university requests to add to the Study Programs application.\nConfirm the request here: https://study-programs.informacni-bezpecnost.cz/admin \n\n";
			$headers = 	'Reply-To: marek.sikora@vut.cz' . "\r\n" .
									'X-Mailer: PHP/' . phpversion();

			if(!mail($to, $subject, $message, $headers)) {
				$output->email = 'Mailer Error: ' . $mail->ErrorInfo;
			} else {
				$output->email = 'Mail has been sent';
			}
		}
	}
}
/* -------------------------------- */



/* -------- HTTP RESPONSE -------- */
$response = json_encode($output);
echo $response;
//http_response_code(406);

if ($output->error == false) unlink("log/".$logfilename.".txt");
/* -------------------------------- */

?>
