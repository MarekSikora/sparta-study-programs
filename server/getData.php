<?php
/* GET request */
header("Access-Control-Allow-Origin: *");

if (isset($_GET["p"]) && (base64_decode($_GET["p"]) == "gimmeData" || base64_decode($_GET["p"]) == "gimmeFullData" || base64_decode($_GET["p"]) == "gimmeRequests")){
	
	$action = base64_decode($_GET['p']);
	$files = ($action == "gimmeRequests") ? glob('./universities/add-requests/*.txt', GLOB_BRACE) : glob('./universities/*.txt', GLOB_BRACE); 
		
	$index = 0;
	$output = [];

	foreach($files as $file) {
	  
		// cteni souboru
		$myfile = fopen($file, "r") or die("Unable to open file!");
		$newObj = json_decode( fread($myfile, filesize($file)));
		
		
		// priprava dat
		$newObj->id = $index;
		if ($action == "gimmeData" && property_exists($newObj, 'created')) unset($newObj->created);
		array_push($output, $newObj);
		fclose($myfile);
		$index++;
		
	}

	echo $json_string = json_encode($output, JSON_PRETTY_PRINT);

} else if (isset($_GET["action"]) && $_GET["action"] == "gimmeData"){
	// old sparta method - remove after update !!!!!
	
	$files = glob('./universities/*.txt', GLOB_BRACE); 
	$index = 0;
	$output = [];

	foreach($files as $file) {
	  
		// cteni souboru
		$myfile = fopen($file, "r") or die("Unable to open file!");
		$newObj = json_decode( fread($myfile, filesize($file)));
		
		// priprava dat
		$newObj->id = $index;
		//unset($newObj->created);
		array_push($output, $newObj);
		fclose($myfile);
		$index++;
		
	}

	echo $json_string = json_encode($output, JSON_PRETTY_PRINT);
	
} else {
	http_response_code(403);
}

?>

