<?php

/* -------- HTTP HEADER -------- */
header("Access-Control-Allow-Origin: *");
/* -------------------------------- */



/* -------- ERROR LOGGING -------- */
$output = new stdClass();
$output->error = false;
$output->message = "ok";

$json = file_get_contents('php://input');
$files = glob('./universities/*.txt', GLOB_BRACE);

if (!$json){
	$output->error = true;
	$output->message = "Can not read the data!";
}
/* -------------------------------- */

// ------ dočasný disabled
	// $output->error = false;
	// $output->message = "Function disabled";
	// $response = json_encode($output);
	// echo $response;
	// exit();
// -------------

/* -------- INPUT DATA READ -------- */
if (!$output->error){	
	$obj = json_decode($json);
	if (property_exists($obj, "universities") && property_exists($obj, "fileName") && property_exists($obj, "action")) {
		$newData = json_decode($obj->{"universities"});
		$newName = $newData->name;
		$oldFileName = $obj->{"fileName"};
		$action = $obj->{"action"};
		if (property_exists($newData, "id")) unset($newData->id);
	} else {
		$output->error = true;
		$output->message = "Can not read the data!asdsad";
	}
}
/* -------------------------------- */


/* -------- DATA IMPORT ---------- */
if (!$output->error){

	// oveření, že soubor už neexistuje
	$exists = false;
	foreach($files as $file){

		if (strpos($file, $newData->name) !== false) {
			$exists = true;
			break;
		}
	}	
	
	if (!$exists) {
		
		if ($action == "publish") unlink('./universities/add-requests/'.$oldFileName.'.txt');
		
		// vytvoření nového txt
		$newFile = fopen("universities/".$newName.".txt", "w");
		if (!$newFile){
			$output->error = true;
			$output->message = "Can not create new txt file!";
		} else {
			// uložení dat
			fwrite($newFile, json_encode($newData, JSON_PRETTY_PRINT));
			fclose($newFile);
		}
		
	} else {
		$output->error = true;
		$output->message = "Can not save new university! A university with the same name already exists.";
	}

}
/* -------------------------------- */



/* -------- HTTP RESPONSE -------- */
$response = json_encode($output);
echo $response;
//http_response_code(406);
/* -------------------------------- */

?>
