<?php
/* POST request */
/* -------- HTTP HEADER -------- */
header("Access-Control-Allow-Origin: *");
/* -------------------------------- */



/* -------- ERROR LOGGING -------- */
$output = new stdClass();
$output->error = false;
$output->message = "ok";

// ------ dočasný disabled
	// $output->error = false;
	// $output->message = "Function disabled";
	// $response = json_encode($output);
	// echo $response;
	// exit();
// -------------

$json = file_get_contents('php://input');
if (!$json){
	$output->error = true;
	$output->message = "Can not read the data!";
}
/* -------------------------------- */



/* -------- INPUT DATA READ -------- */
if (!$output->error){	
	$obj = json_decode($json);
	if (property_exists($obj, "universities") && property_exists($obj, "fileName") && property_exists($obj, "action")) {
		$newData = json_decode($obj->{"universities"});
		$oldFileName = $obj->{"fileName"};
		$action = $obj->{"action"};
		if (property_exists($newData, "id")) unset($newData->id);
	} else {
		$output->error = true;
		$output->message = "An error occurred while writing to the database!";
	}
}
/* -------------------------------- */



/* -------- READ DB FILES -------- */
if (!$output->error){
	$files = glob('./universities/'.$oldFileName.'.txt', GLOB_BRACE);
	if (count($files) != 1 || $action != "overwrite") {
		$output->error = true;
		$output->message = "An error occurred while writing to the database!";
	}
}
/* -------------------------------- */



/* -------- DATA IMPORT ---------- */ 
if (!$output->error){
	
	// smazani stareho souboru
	unlink('./universities/'.$oldFileName.'.txt');
	
	// přepsání nového txt
	$newName = $newData->name;
	$newFile = fopen("universities/".$newName.".txt", "w");
	if (!$newFile){
		$output->error = true;
		$output->message = "Can not create new txt file!";
	}
	// vložení json dat
	fwrite($newFile, json_encode($newData, JSON_PRETTY_PRINT));
	fclose($newFile);
}
/* -------------------------------- */



/* -------- HTTP RESPONSE -------- */
$response = json_encode($output);
echo $response;
//http_response_code(406);
/* -------------------------------- */

?>
